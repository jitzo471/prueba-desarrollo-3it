# Prueba de desarrollo - [Luis Miguel Benavides](mailto://jitzo471@gmail.com)

Desarrollo de prueba con Framework Java Spring Boot en el Backend y Angular 8 en el Frontend.

La web está compuesta por 3 páginas: Landing, Formulario y Resultados.

### Instrucciones

En orden para correr el proyecto se requiere:

*   En un cliente de MySQL crear una BD en blanco llamada **prueba_desarrollo**
*   Clonar el repositorio: git clone https://gitlab.com/jitzo471/prueba-desarrollo-3it.git
*   Dependiendo del tipo de autenticación que se tenga en el cliente cambiar los valores para **spring.datasource.username** y **spring.datasource.password** del archivo **application.properties**
*   Abrir una terminal de comandos en el directorio **Front** e instalar sus dependencias **npm install**
*   Ejecutar el proyecto **Front** con **npm start**
*   Ejecutar el proyecto **Back** con el IDE de su gusto para Java Spring Boot

En la clase principal del Backend se implantan una pequeña base de tipos de estilos musicales que servirán al momento de realizar el registro, pero no necesariamente el usuario tiene que elegir alguna de esas opciones.

Si se selecciona la opción **Otro** el usuario podrá ingresar el estilo musical de su agrado.
