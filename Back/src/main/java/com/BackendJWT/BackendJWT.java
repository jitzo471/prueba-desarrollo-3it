package com.BackendJWT;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

import com.BackendJWT.api.entities.MusicStyle;
import com.BackendJWT.api.service.MusicStyleDao;

@SpringBootApplication
public class BackendJWT extends SpringBootServletInitializer {

	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(BackendJWT.class);
    }
	
	public static void main(String[] args) {
		SpringApplication.run(BackendJWT.class, args);
	}
	
	@Bean
	CommandLineRunner init(MusicStyleDao musicStyleDao) {
		return args -> {
			initMusicStyles	(musicStyleDao);
		};
	}
	
	private void initMusicStyles(MusicStyleDao musicStyleDao) {
		MusicStyle musicStyle = new MusicStyle();
		musicStyle.setMusicStyleId((long)1);
		musicStyle.setName("Rock");
		musicStyle.setDescription("Estilos de música popular originados como rock and roll a principios de la década de 1950.");
		MusicStyle find = musicStyleDao.findByMusicStyleId(musicStyle.getMusicStyleId());
		if (find == null) {
			musicStyleDao.save(musicStyle);
		}

		musicStyle = new MusicStyle();
		musicStyle.setMusicStyleId((long)2);
		musicStyle.setName("Pop");
		musicStyle.setDescription("Tuvo su origen a finales de los años 1950 como una derivación del Traditional Pop, en combinación con otros géneros musicales que estaban de moda en aquel momento.");
		find = musicStyleDao.findByMusicStyleId(musicStyle.getMusicStyleId());
		if (find == null) {
			musicStyleDao.save(musicStyle);
		}

		musicStyle = new MusicStyle();
		musicStyle.setMusicStyleId((long)3);
		musicStyle.setName("Jazz");
		musicStyle.setDescription("Género musical nacido a finales del siglo XIX en los Estados Unidos, que se expandió de forma global a lo largo del siglo XX.");
		find = musicStyleDao.findByMusicStyleId(musicStyle.getMusicStyleId());
		if (find == null) {
			musicStyleDao.save(musicStyle);
		}

		musicStyle = new MusicStyle();
		musicStyle.setMusicStyleId((long)4);
		musicStyle.setName("Clásica");
		musicStyle.setDescription("Corriente musical que se basa principalmente en la música producida o basada en las tradiciones de la música litúrgica y secular de Occidente, principalmente Europa Occidental.");
		find = musicStyleDao.findByMusicStyleId(musicStyle.getMusicStyleId());
		if (find == null) {
			musicStyleDao.save(musicStyle);
		}
	}
}
