package com.BackendJWT.api.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.BackendJWT.api.entities.User;
import com.BackendJWT.api.response.GeneralRest;
import com.BackendJWT.api.service.UserDao;

@RestController
@RequestMapping("/api/user")
@CrossOrigin(origins = "*")
public class UserController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private UserDao userDao;

	/**
	 * Función que permite actualizar un vecino
	 * 
	 * @param userTmp Objeto con la información del usuario
	 * @return ResponseEntity
	 */
	@PostMapping("/store")
	public ResponseEntity<?> saveNeighbor(@RequestBody User userTmp) {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		String message = "";
		try {
			User user = userDao.findByEmail(userTmp.getEmail());

			if (user == null) {
				user = userTmp;
				userDao.save(user);
				response.put("user", user);
				response.put("success", true);
				message = "Usuario registrado correctamente";
			} else {
				response.put("success", false);
				message = "Usuario ya existe";
			}

			GeneralRest<?> generalRest = new GeneralRest<>(response, message, 200);
			return new ResponseEntity<GeneralRest<?>>(generalRest, HttpStatus.OK);
		} catch (Exception e) {
			GeneralRest<?> generalRest = new GeneralRest<>(e, "Ha ocurrido un error interno en el sistema", 500);
			return new ResponseEntity<GeneralRest<?>>(generalRest, HttpStatus.BAD_GATEWAY);
		}
	}

	/**
	 * Función que obtiene todos los usuarios registrados
	 * 
	 * @return ResponseEntity
	 */
	@GetMapping("/getAll")
	public ResponseEntity<?> getAll() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		try {
			Iterable<User> userList = userDao.findAll();
			response.put("success", true);
			response.put("users", userList);

			GeneralRest<?> generalRest = new GeneralRest<>(response, "Información obtenida correctamente", 200);
			return new ResponseEntity<GeneralRest<?>>(generalRest, HttpStatus.OK);
		} catch (Exception e) {
			GeneralRest<?> generalRest = new GeneralRest<>(e, "Ha ocurrido un error interno en el sistema", 500);
			return new ResponseEntity<GeneralRest<?>>(generalRest, HttpStatus.BAD_GATEWAY);
		}
	}
}
