package com.BackendJWT.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedHashMap;

import com.BackendJWT.api.entities.MusicStyle;
import com.BackendJWT.api.response.GeneralRest;
import com.BackendJWT.api.service.MusicStyleDao;

@RestController
@RequestMapping("/api/musicStyle")
@CrossOrigin(origins = "*")
public class MusicStyleController {
	
	@Autowired
	private MusicStyleDao musicStyleDao;

	/**
	 * Función que obtiene todos los estilos de música precargados
	 * 
	 * @return ResponseEntity
	 */
	@GetMapping("/getAll")
	public ResponseEntity<?> getAll() {
		LinkedHashMap<String, Object> response = new LinkedHashMap<>();
		try {
			Iterable<MusicStyle> musicStyles = musicStyleDao.findAll();
			response.put("success", true);
			response.put("musicStyles", musicStyles);

			GeneralRest<?> generalRest = new GeneralRest<>(response, "Información obtenida correctamente", 200);
			return new ResponseEntity<GeneralRest<?>>(generalRest, HttpStatus.OK);
		} catch (Exception e) {
			GeneralRest<?> generalRest = new GeneralRest<>(e, "Ha ocurrido un error interno en el sistema", 500);
			return new ResponseEntity<GeneralRest<?>>(generalRest, HttpStatus.BAD_GATEWAY);
		}
	}
}
