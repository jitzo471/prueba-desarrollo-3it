package com.BackendJWT.api.entities;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name = "music_styles")
@TableGenerator(name = "tab", initialValue = 1, allocationSize = 1)
public class MusicStyle implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Basic(optional = false)
	@Column(name = "id", unique = true, nullable = false)
	private Long musicStyleId;
	
	@Column(name = "name")
	@NotNull
	private String name;
	
	@Column(name = "desciption")
	@NotNull
	private String description;
	
	@Column(name = "creation_date")
	@CreationTimestamp
	private Date creationDate;
	
	public MusicStyle() {
		
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Long getMusicStyleId() {
		return this.musicStyleId;
	}

	public void setMusicStyleId(Long musicStyleId) {
		this.musicStyleId = musicStyleId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

}
