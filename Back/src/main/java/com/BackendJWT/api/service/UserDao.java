package com.BackendJWT.api.service;

import javax.transaction.Transactional;
import org.springframework.data.repository.CrudRepository;
import com.BackendJWT.api.entities.User;

@Transactional
public interface UserDao extends CrudRepository<User, Long> {
    public User  findByEmail(String email);
}
