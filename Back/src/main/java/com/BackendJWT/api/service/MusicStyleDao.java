package com.BackendJWT.api.service;

import javax.transaction.Transactional;
import org.springframework.data.repository.CrudRepository;
import com.BackendJWT.api.entities.MusicStyle;

@Transactional
public interface MusicStyleDao extends CrudRepository<MusicStyle, Long>{

	public MusicStyle findByMusicStyleId(Long id);
}
