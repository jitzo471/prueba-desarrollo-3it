import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { ResultsRoutingModule } from './results-routing.module';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    ResultsRoutingModule
  ],
  declarations: []
})
export class ResultsModule { }
