import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';
import { CrudServices } from '../shared/services/crud-service';
import swal from 'sweetalert2';
import { User } from '../shared/models/user';

@Component({
    selector: 'app-results',
    templateUrl: './results.component.html',
    styleUrls: ['./results.component.scss'],
    animations: [routerTransition()]
})
export class ResultsComponent implements OnInit {
    public results: any = [];

    constructor(private crudService: CrudServices) {
    }

    ngOnInit() {
        this.getResults();
    }

    // Obtiene los resultados del formulario
    getResults() {
        this.crudService.getRequest('/user/getAll').subscribe(
            data => {
                const json: any = data;
                if (json.genericObject.success) {
                    this.results = json.genericObject.users;
                } else {
                    swal.fire('ATENCIÓN!', json.genericObject.message, 'error');
                }
            },
            error => {
                swal.fire('ERROR EN EL REGISTRO!', 'Ha ocurrido un error', 'error');
                console.error(error);
            }
        );
    }
}
