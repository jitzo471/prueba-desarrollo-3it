import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment.prod';
import { GlobalApp } from '../shared/services/globalApp';

@Component({
  selector: 'app-navbar-user',
  templateUrl: './navbar-user.component.html',
  styleUrls: ['./navbar-user.component.scss']
})
export class NavbarUserComponent implements OnInit {

    userName: any = '';
    isUser: boolean = environment.isUser;

    constructor(
        private router: Router,
        public app: GlobalApp
    ) { }

    ngOnInit() {
        this.userName = environment.userName;
    }

    /**
     * Función para cerrar la sesión del usuario
     */
    onLoggedout() {
        window.sessionStorage.clear();
        this.router.navigate(['/']);
    }

}
