import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NavbarUserComponent } from './navbar-user.component';

const routes: Routes = [
    {
        path: '',
        component: NavbarUserComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class NavbarUserRoutingModule {
}
