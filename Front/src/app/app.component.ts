import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment.prod';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    constructor() {
    }

    ngOnInit() {
        if (window.sessionStorage.getItem('AUTH_ROLE') === 'Donante') {
            environment.isUser = true;
            environment.userName = window.sessionStorage.getItem('AUTH_NAME');
        }
    }
}
