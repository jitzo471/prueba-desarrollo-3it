import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FooterUserRoutingModule } from './footer-user-routing.module';

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        FooterUserRoutingModule
    ],
    declarations: []
})
export class FooterUserModule {}
