import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FooterUserComponent } from './footer-user.component';

const routes: Routes = [
    {
        path: '',
        component: FooterUserComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class FooterUserRoutingModule {
}
