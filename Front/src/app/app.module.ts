import { CommonModule, LocationStrategy, HashLocationStrategy } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LanguageTranslationModule } from './shared/modules/language-translation/language-translation.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AuthGuard } from './shared';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NavbarUserComponent } from './navbar-user/navbar-user.component';
import { FooterUserComponent } from './footer-user/footer-user.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { HomePageComponent } from './home-page/home-page.component';
import { FormsModule } from '@angular/forms';
import * as $ from 'jquery';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { PrivacityPoliceComponent } from './privacity-police/privacity-police.component';
import { GlobalApp } from './shared/services/globalApp';

import { SignupComponent } from './signup/signup.component';
import { ResultsComponent } from './results/results.component';
@NgModule({
    imports: [
        NgbModule,
        CommonModule,
        FormsModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        LanguageTranslationModule,
        AppRoutingModule,
        SlickCarouselModule
    ],
    declarations: [
        AppComponent,
        NavbarUserComponent,
        FooterUserComponent,
        NotFoundComponent,
        HomePageComponent,
        PrivacityPoliceComponent,
        SignupComponent,
        ResultsComponent
    ],
    providers: [
        AuthGuard,
        GlobalApp,
        { provide: LocationStrategy, useClass: HashLocationStrategy }],
    entryComponents: [
        PrivacityPoliceComponent,
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
