export class GlobalApp {

    constructor(
    ) {}

    public getItem(id: string): string {
        return sessionStorage.getItem(id);
    }

    public logout() {
      sessionStorage.clear();
      window.location.href = '/';
    }
  }
