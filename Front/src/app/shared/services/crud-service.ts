import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })

export class CrudServices {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  serverURL = environment.serverUrl;

  constructor(private http: HttpClient) {
      if (this.isAuthenticated()) {
        this.buildHeaderAuthenticated();
      }
  }

  async buildHeaderAuthenticated() {
    // const token = this.tokenStorage.getToken();
    this.httpOptions = {
      headers: await new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + sessionStorage.getItem('api_key')
      })
    };
  }

  isAuthenticated() {
    // here you can check if user is authenticated or not through his token
    if (sessionStorage.getItem('api_key') != null) {
      this.buildHeaderAuthenticated();
      return true;
    } else {
      this.buildHeaderAuthenticated();
      return false;
    }
  }

  /*************************** Servicios CRUD para la BD ***************************/

  // Solicitud GET
  getRequest(path): Observable<string> {
    return this.http.get<string>(this.serverURL + path, this.httpOptions);
  }

  // Crea un elemento
  postRequest(info, path): Observable<string> {
    return this.http.post<string>(`${this.serverURL}${path}`, info, this.httpOptions);
  }

  //Eliminación de un elemento
  deleteRequest(path): Observable<string> {
    return this.http.delete<string>(this.serverURL + path, this.httpOptions);
  }

  authenticate(credentials): Observable<string> {
    return this.http.post<string>(this.serverURL + 'api/authenticate', credentials, this.httpOptions);
  }

}
