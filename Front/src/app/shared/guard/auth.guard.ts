import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private router: Router) {}

    canActivate() {
        console.log('ENTRO A VALIDAR USUARIO');

        if (window.sessionStorage.getItem('api_key')) {
            // this.router.navigate(['/dashboard-admin']);
            return true;
        }

        this.router.navigate(['/registrarse']);
        return false;
    }
}
