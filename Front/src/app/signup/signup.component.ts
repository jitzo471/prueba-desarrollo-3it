import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../router.animations';
import { CrudServices } from '../shared/services/crud-service';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { JsonService } from '../shared/services/json-service';
import { User } from '../shared/models/user';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss'],
    animations: [routerTransition()]
})
export class SignupComponent implements OnInit {
    public user = new User;
    public musicStyles: any = [];
    public otherMusicFlag: boolean = false;
    public otherMusic: string = '';

    constructor(private crudService: CrudServices, private jsonService: JsonService, private router: Router) {
    }

    ngOnInit() {
        $('#email').on('keyup change', function (e) {
            const emailRegex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (emailRegex.test($('#email').val() + '')) {
                this.style.backgroundColor = '';
            } else {
                this.style.backgroundColor = 'LightPink';
            }
        });
        this.getMusicStyles();
        this.user.musicStyle = '';
    }

    // Obtiene lista de estilos de música
    getMusicStyles() {
        this.crudService.getRequest('/musicStyle/getAll').subscribe(
            data => {
                const json: any = data;
                if (json.genericObject.success) {
                    this.musicStyles = json.genericObject.musicStyles;
                } else {
                    swal.fire('ATENCIÓN!', json.genericObject.message, 'error');
                }
            },
            error => {
                swal.fire('ERROR!', 'Ha ocurrido un error', 'error');
                console.error(error);
            }
        );
    }

    // Realiza el registro del usuario en el sistema
    onSubmit() {
        if (this.user.musicStyle === 'otro') {
            this.user.musicStyle = this.otherMusic;
        }
        this.crudService.postRequest(this.user, '/user/store').subscribe(
            data => {
                const json: any = data;
                if (json.genericObject.success) {
                    swal.fire('EXCELENTE!', 'Te has registrado correctamente.', 'success');
                    this.router.navigateByUrl('resultados');
                } else {
                    swal.fire('ATENCIÓN!', json.genericObject.message, 'error');
                }
            },
            error => {
                swal.fire('ERROR EN EL REGISTRO!', 'Ha ocurrido un error', 'error');
                console.error(error);
            }
        );

    }

    // Valida si un input está vacío
    validateEmptyInput(input) {
        if (input === null || !input) {
            return true;
        }
        return false;
    }

    //Valida que el email cumpla con el patrón adecuado
    validateEmailInput(email) {
        const emailRegex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (email && !emailRegex.test(email)) {
            return true;
        }
        return false;
    }

    changeOther() {
        if (this.user.musicStyle === 'otro') {
            this.otherMusicFlag = true;
        } else {
            this.otherMusicFlag = false;
        }
    }
}
