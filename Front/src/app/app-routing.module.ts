import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomePageComponent } from './home-page/home-page.component';
import { SignupComponent } from './signup/signup.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { PrivacityPoliceComponent } from './privacity-police/privacity-police.component';
import { ResultsComponent } from './results/results.component';

const routes: Routes = [
    { path: '', component: HomePageComponent },
    { path: 'registrarse', component: SignupComponent },
    { path: 'resultados', component: ResultsComponent },
    { path: 'politica-privacidad', component: PrivacityPoliceComponent },
    { path: '**', component: NotFoundComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
